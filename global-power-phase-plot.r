## Global-power-phase-plot
#This graphic is published under the modified 3-clause BSD license
#Author: José Luis García Pallero, jgpallero@gmail.com
#Load season metrics
m <- GC.metrics()
#Variables to compute
alpps <- 0
alppe <- 0
arpps <- 0
arppe <- 0
alppps <- 0
alpppe <- 0
arppps <- 0
arpppe <- 0
nl <- 0
nr <- 0
#Loop over all activities
for(i in 1:length(m$Average_Left_Power_Phase_End))
{
    #Check if it is a valid data for the left leg
    if((m$Average_Left_Power_Phase_End[i]!=0)&(m$Average_Left_Peak_Power_Phase_End[i]!=0))
    {
        nl <- nl+1
        alpps <- alpps+m$Average_Left_Power_Phase_Start[i]
        alppe <- alppe+m$Average_Left_Power_Phase_End[i]
        alppps <- alppps+m$Average_Left_Peak_Power_Phase_Start[i]
        alpppe <- alpppe+m$Average_Left_Peak_Power_Phase_End[i]
    }
    #Check if it is a valid data for the right leg
    if((m$Average_Right_Power_Phase_End[i]!=0)&(m$Average_Right_Peak_Power_Phase_End[i]!=0))
    {
        nr <- nr+1
        arpps <- arpps+m$Average_Right_Power_Phase_Start[i]
        arppe <- arppe+m$Average_Right_Power_Phase_End[i]
        arppps <- arppps+m$Average_Right_Peak_Power_Phase_Start[i]
        arpppe <- arpppe+m$Average_Right_Peak_Power_Phase_End[i]
    }
}
#Compute the mean values
if(nl>0)
{
    alpps <- alpps/nl
    alppe <- alppe/nl
    alppps <- alppps/nl
    alpppe <- alpppe/nl
}
if(nr>0)
{
    arpps <- arpps/nr
    arppe <- arppe/nr
    arppps <- arppps/nr
    arpppe <- arpppe/nr
}
################################################################################
# function to create a circle
circle <- function(center=c(0,0),radius=1,npoints=360)
{
    r = radius
    tt = seq(0,2*pi,length=npoints)
    xx = center[1]+r*cos(tt)
    yy = center[2]+r*sin(tt)
    return(data.frame(x=xx,y=yy))
}

# function to get slices
slice2xy <- function(t,rad)
{
    t2p = -t*pi/180+pi/2
    list(x=rad*cos(t2p),y=rad*sin(t2p))
}

# function to draw slices
drawSlice <- function(Pstart=0,Pend=180,rad=1.0,Pcolor="red")
{
    S = slice2xy(seq.int(Pstart,Pend,length.out=30),rad)
    polygon(c(S$x,0),c(S$y,0),border=Pcolor,col=Pcolor,lty=NULL)
}

# function to get major and minor tick marks
ticks <- function(center=c(0,0),from=0,to=2*pi,radius=0.9,npoints=5)
{
    r = radius
    tt = seq(from,to,length=npoints)
    xx = center[1]+r*cos(tt)
    yy = center[2]+r*sin(tt)
    return(data.frame(x=xx,y=yy))
}

# function to label start and end points
labelPosition <- function(degrees=0,radius=0.9,color="green")
{
    v0 = -degrees*pi/180+pi/2
    Pos0 = list(x=radius*cos(v0),y=radius*sin(v0))
    text(Pos0,labels=degrees,col=color,cex=1.5)
}

# function to plot radial gauge chart
plotRGC <- function (Pstart=350,Pend=220,PPstart=61,PPend=120,Pedal="Left leg")
{
    #using round() instead of floor()
    Pstart = round(Pstart)
    Pend = round(Pend)
    PPstart = round(PPstart)
    PPend = round(PPend)

    # external circle (this will be used for the black border)
    border_cir = circle(c(0,0),radius=1,npoints=360)


    # coordinates of major ticks (will be plotted as arrows)
    major_ticks_out = ticks(c(0,0),from=0,to=3*pi/2,radius=1.0,4)
    major_ticks_in = ticks(c(0,0),from=0,to=3*pi/2,radius=0.8,4)

    # open plot
    plot(border_cir$x,border_cir$y,type="n",asp=1,axes=FALSE,
         xlim=c(-1.05,1.05),ylim=c(-1.05,1.05),xlab="",ylab="")

    # draw slices
    drawSlice(Pstart,Pend,rad=1.0,Pcolor="orange")
    drawSlice(Pstart,Pend,rad=0.8,Pcolor="white")
    drawSlice(PPstart,PPend,rad=0.8,Pcolor="red")
    drawSlice(Pstart,Pend,rad=0.6,Pcolor="white")

    # add external border
    lines(border_cir$x,border_cir$y,col="black",lwd=4)

    # add major ticks
    arrows(x0=major_ticks_out$x,y0=major_ticks_out$y,
           x1=major_ticks_in$x,y1=major_ticks_in$y,length=0,lwd=4)

    # add label of pedal
    text(0,1.2,Pedal,cex=1.5,font=2)
    text(0.1,0.0,"Peak",cex=1.5,col="red")

    # Postion labels
    labelPosition(Pstart%%360,0.7,"orange")
    labelPosition(Pend,0.7,"orange")
    labelPosition(PPstart%%360,0.45,"red")
    labelPosition(PPend,0.45,"red")
}################################################################################
#Plotting results
par(mfrow=c(1,2),mar=c(0,0,0,0))
plotRGC(alpps,alppe,alppps,alpppe,Pedal="Left leg")
plotRGC(arpps,arppe,arppps,arpppe,Pedal="Right leg")
################################################################################
#Copyright (c) 2019-2021, José Luis García Pallero, jgpallero@gmail.com
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#   Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#   Neither the name of copyright holders nor the names of its contributors may
#   be used to endorse or promote products derived from this software without
#   specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
