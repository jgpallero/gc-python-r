#####################################BALANCE####################################
{
	init
	{
		dato <-
%%python
import numpy as np
from math import floor
#Límites del balance
bmin = 20.0
bmax = 80.0
#Cargo los datos de trabajo
poto = np.asarray(GC.series(GC.SERIES_WATTS))
cado = np.asarray(GC.series(GC.SERIES_CAD))
lrbo = np.asarray(GC.series(GC.SERIES_LRBALANCE))
efio = np.asarray(GC.series(GC.SERIES_LTE))
efdo = np.asarray(GC.series(GC.SERIES_RTE))
flio = np.asarray(GC.series(GC.SERIES_LPS))
fldo = np.asarray(GC.series(GC.SERIES_RPS))
#Asigno valores 0.0 a los posibles valores -255 que se leen del fichero cuando
#alguna entrada del campo de balance está vacía
pos = lrbo<0.0
if sum(pos)>0:
	lrbo[pos] = 0.0
#Si no hay balance puede ser que el potenciómetro sea de una sola pierna, en
#cuyo caso asigno el valor 50%
if sum(lrbo)==0.0:
	#No hace falta calcular más
	GC.result(50*1000000+0)
#Buscamos los datos válidos (a partir de aquí la cadencia ya no se usa más)
pos = (cado>0)&(poto>0)&(lrbo>bmin)&(lrbo<bmax)
#No todos los potenciómetros almacenan efectividad y fluidez, por lo que hay que
#detectar si están presentes para luego usalos (si la serie no existe
#GoldenCheetah devuelve un vector de ceros al llamarla con GC.series)
hayEfio = 0;
hayEfdo = 0;
hayFlio = 0;
hayFldo = 0;
if sum(efio)>0.0:
	pos &= (efio>0)
	hayEfio = 1;
if sum(efdo)>0.0:
	pos &= (efdo>0)
	hayEfdo = 1;
if sum(flio)>0.0:
	pos &= (flio>0)
	hayFlio = 1;
if sum(fldo)>0.0:
	pos &= (fldo>0)
	hayFldo = 1;
#Comprobamos si hay datos
if sum(pos)>0:
	#Voy a almacenar el sumatorio de las potencias y el número de elementos del
	#vector como C=A*X+B, donde X será un factor igual a 1000000, que es
	#perfectamente válido en la práctica porque nunca va a haber un vector de
	#ese número de datos. El valor A es el sumatorio de las potencias
	#multiplicadas por 100 y redondeadas para así quedarme con tres decimales y
	#el valor B es el número de elementos del vector
	#Convierto los datos a enteros incluyendo tres decimales
	lrbo = np.round(lrbo[pos]*1000)
	#El sumatorio de las potencias lo multiplico por 1000000
	GC.result(np.sum(lrbo)*1000000+len(lrbo))
else:
	GC.result(50*1000000+0)
%% ;
		#Extraigo el sumatorio de los valores
		suma <- floor(dato/1000000);
		#Extraigo el número de datos
		n <- dato-suma*1000000;
	}
	#Media de los datos originales (recupero los decimales) y el número de datos
    value{ suma/n/1000; }
    count{ n; }
}
##############################EFECTIVIDAD IZQUIERDA#############################
{
	init
	{
		dato <-
%%python
import numpy as np
#Límites del balance
bmin = 20.0
bmax = 80.0
#Cargo los datos de trabajo
poto = np.asarray(GC.series(GC.SERIES_WATTS))
cado = np.asarray(GC.series(GC.SERIES_CAD))
lrbo = np.asarray(GC.series(GC.SERIES_LRBALANCE))
efio = np.asarray(GC.series(GC.SERIES_LTE))
efdo = np.asarray(GC.series(GC.SERIES_RTE))
flio = np.asarray(GC.series(GC.SERIES_LPS))
fldo = np.asarray(GC.series(GC.SERIES_RPS))
#Asigno valores 0.0 a los posibles valores -255 que se leen del fichero cuando
#alguna entrada del campo de balance está vacía
pos = lrbo<0.0
if sum(pos)>0:
	lrbo[pos] = 0.0
#Si no hay balance puede ser que el potenciómetro sea de una sola pierna, en
#cuyo caso asigno el valor 50%
if sum(lrbo)==0.0:
	lrbo = np.ones(len(poto))*50.0
#Buscamos los datos válidos (a partir de aquí la cadencia ya no se usa más)
pos = (cado>0)&(poto>0)&(lrbo>bmin)&(lrbo<bmax)
#No todos los potenciómetros almacenan efectividad y fluidez, por lo que hay que
#detectar si están presentes para luego usalos (si la serie no existe
#GoldenCheetah devuelve un vector de ceros al llamarla con GC.series)
hayEfio = 0;
hayEfdo = 0;
hayFlio = 0;
hayFldo = 0;
if sum(efio)>0.0:
	pos &= (efio>0)
	hayEfio = 1;
if sum(efdo)>0.0:
	pos &= (efdo>0)
	hayEfdo = 1;
if sum(flio)>0.0:
	pos &= (flio>0)
	hayFlio = 1;
if sum(fldo)>0.0:
	pos &= (fldo>0)
	hayFldo = 1;
#Comprobamos si hay datos
if hayEfio and (sum(pos)>0):
	#Voy a almacenar el sumatorio de las efectividades y el número de elementos
	#del vector como C=A*X+B, donde X será un factor igual a 1000000, que es
	#perfectamente válido en la práctica porque nunca va a haber un vector de
	#ese número de datos. El valor A es el sumatorio de las efectividades
	#multiplicadas por 100 y redondeadas para así quedarme con tres decimales y
	#el valor B es el número de elementos del vector
	#Convierto los datos a enteros incluyendo tres decimales
	efio = np.round(efio[pos]*1000)
	#El sumatorio de las efectividades lo multiplico por 1000000
	GC.result(np.sum(efio)*1000000+len(efio))
else:
	GC.result(0)
%% ;
		#Extraigo el sumatorio de los valores
		suma <- floor(dato/1000000);
		#Extraigo el número de datos
		n <- dato-suma*1000000;
	}
	#Media de los datos originales (recupero los decimales) y el número de datos
    value{ suma/n/1000; }
    count{ n; }
}
###############################EFECTIVIDAD DERECHA##############################
{
	init
	{
		dato <-
%%python
import numpy as np
#Límites del balance
bmin = 20.0
bmax = 80.0
#Cargo los datos de trabajo
poto = np.asarray(GC.series(GC.SERIES_WATTS))
cado = np.asarray(GC.series(GC.SERIES_CAD))
lrbo = np.asarray(GC.series(GC.SERIES_LRBALANCE))
efio = np.asarray(GC.series(GC.SERIES_LTE))
efdo = np.asarray(GC.series(GC.SERIES_RTE))
flio = np.asarray(GC.series(GC.SERIES_LPS))
fldo = np.asarray(GC.series(GC.SERIES_RPS))
#Asigno valores 0.0 a los posibles valores -255 que se leen del fichero cuando
#alguna entrada del campo de balance está vacía
pos = lrbo<0.0
if sum(pos)>0:
	lrbo[pos] = 0.0
#Si no hay balance puede ser que el potenciómetro sea de una sola pierna, en
#cuyo caso asigno el valor 50%
if sum(lrbo)==0.0:
	lrbo = np.ones(len(poto))*50.0
#Buscamos los datos válidos (a partir de aquí la cadencia ya no se usa más)
pos = (cado>0)&(poto>0)&(lrbo>bmin)&(lrbo<bmax)
#No todos los potenciómetros almacenan efectividad y fluidez, por lo que hay que
#detectar si están presentes para luego usalos (si la serie no existe
#GoldenCheetah devuelve un vector de ceros al llamarla con GC.series)
hayEfio = 0;
hayEfdo = 0;
hayFlio = 0;
hayFldo = 0;
if sum(efio)>0.0:
	pos &= (efio>0)
	hayEfio = 1;
if sum(efdo)>0.0:
	pos &= (efdo>0)
	hayEfdo = 1;
if sum(flio)>0.0:
	pos &= (flio>0)
	hayFlio = 1;
if sum(fldo)>0.0:
	pos &= (fldo>0)
	hayFldo = 1;
#Comprobamos si hay datos
if hayEfdo and (sum(pos)>0):
	#Voy a almacenar el sumatorio de las efectividades y el número de elementos
	#del vector como C=A*X+B, donde X será un factor igual a 1000000, que es
	#perfectamente válido en la práctica porque nunca va a haber un vector de
	#ese número de datos. El valor A es el sumatorio de las efectividades
	#multiplicadas por 100 y redondeadas para así quedarme con tres decimales y
	#el valor B es el número de elementos del vector
	#Convierto los datos a enteros incluyendo tres decimales
	efdo = np.round(efdo[pos]*1000)
	#El sumatorio de las efectividades lo multiplico por 1000000
	GC.result(np.sum(efdo)*1000000+len(efdo))
else:
	GC.result(0)
%% ;
		#Extraigo el sumatorio de los valores
		suma <- floor(dato/1000000);
		#Extraigo el número de datos
		n <- dato-suma*1000000;
	}
	#Media de los datos originales (recupero los decimales) y el número de datos
    value{ suma/n/1000; }
    count{ n; }
}
################################FLUIDEZ IZQUIERDA###############################
{
	init
	{
		dato <-
%%python
import numpy as np
#Límites del balance
bmin = 20.0
bmax = 80.0
#Cargo los datos de trabajo
poto = np.asarray(GC.series(GC.SERIES_WATTS))
cado = np.asarray(GC.series(GC.SERIES_CAD))
lrbo = np.asarray(GC.series(GC.SERIES_LRBALANCE))
efio = np.asarray(GC.series(GC.SERIES_LTE))
efdo = np.asarray(GC.series(GC.SERIES_RTE))
flio = np.asarray(GC.series(GC.SERIES_LPS))
fldo = np.asarray(GC.series(GC.SERIES_RPS))
#Asigno valores 0.0 a los posibles valores -255 que se leen del fichero cuando
#alguna entrada del campo de balance está vacía
pos = lrbo<0.0
if sum(pos)>0:
	lrbo[pos] = 0.0
#Si no hay balance puede ser que el potenciómetro sea de una sola pierna, en
#cuyo caso asigno el valor 50%
if sum(lrbo)==0.0:
	lrbo = np.ones(len(poto))*50.0
#Buscamos los datos válidos (a partir de aquí la cadencia ya no se usa más)
pos = (cado>0)&(poto>0)&(lrbo>bmin)&(lrbo<bmax)
#No todos los potenciómetros almacenan efectividad y fluidez, por lo que hay que
#detectar si están presentes para luego usalos (si la serie no existe
#GoldenCheetah devuelve un vector de ceros al llamarla con GC.series)
hayEfio = 0;
hayEfdo = 0;
hayFlio = 0;
hayFldo = 0;
if sum(efio)>0.0:
	pos &= (efio>0)
	hayEfio = 1;
if sum(efdo)>0.0:
	pos &= (efdo>0)
	hayEfdo = 1;
if sum(flio)>0.0:
	pos &= (flio>0)
	hayFlio = 1;
if sum(fldo)>0.0:
	pos &= (fldo>0)
	hayFldo = 1;
#Comprobamos si hay datos
if hayFlio and (sum(pos)>0):
	#Voy a almacenar el sumatorio de las fluideces y el número de elementos del
	#vector como C=A*X+B, donde X será un factor igual a 1000000, que es
	#perfectamente válido en la práctica porque nunca va a haber un vector de
	#ese número de datos. El valor A es el sumatorio de las fluideces
	#multiplicadas por 100 y redondeadas para así quedarme con tres decimales y
	#el valor B es el número de elementos del vector
	#Convierto los datos a enteros incluyendo tres decimales
	flio = np.round(flio[pos]*1000)
	#El sumatorio de las fluideces lo multiplico por 1000000
	GC.result(np.sum(flio)*1000000+len(flio))
else:
	GC.result(0)
%% ;
		#Extraigo el sumatorio de los valores
		suma <- floor(dato/1000000);
		#Extraigo el número de datos
		n <- dato-suma*1000000;
	}
	#Media de los datos originales (recupero los decimales) y el número de datos
    value{ suma/n/1000; }
    count{ n; }
}
#################################FLUIDEZ DERECHA################################
{
	init
	{
		dato <-
%%python
import numpy as np
#Límites del balance
bmin = 20.0
bmax = 80.0
#Cargo los datos de trabajo
poto = np.asarray(GC.series(GC.SERIES_WATTS))
cado = np.asarray(GC.series(GC.SERIES_CAD))
lrbo = np.asarray(GC.series(GC.SERIES_LRBALANCE))
efio = np.asarray(GC.series(GC.SERIES_LTE))
efdo = np.asarray(GC.series(GC.SERIES_RTE))
flio = np.asarray(GC.series(GC.SERIES_LPS))
fldo = np.asarray(GC.series(GC.SERIES_RPS))
#Asigno valores 0.0 a los posibles valores -255 que se leen del fichero cuando
#alguna entrada del campo de balance está vacía
pos = lrbo<0.0
if sum(pos)>0:
	lrbo[pos] = 0.0
#Si no hay balance puede ser que el potenciómetro sea de una sola pierna, en
#cuyo caso asigno el valor 50%
if sum(lrbo)==0.0:
	lrbo = np.ones(len(poto))*50.0
#Buscamos los datos válidos (a partir de aquí la cadencia ya no se usa más)
pos = (cado>0)&(poto>0)&(lrbo>bmin)&(lrbo<bmax)
#No todos los potenciómetros almacenan efectividad y fluidez, por lo que hay que
#detectar si están presentes para luego usalos (si la serie no existe
#GoldenCheetah devuelve un vector de ceros al llamarla con GC.series)
hayEfio = 0;
hayEfdo = 0;
hayFlio = 0;
hayFldo = 0;
if sum(efio)>0.0:
	pos &= (efio>0)
	hayEfio = 1;
if sum(efdo)>0.0:
	pos &= (efdo>0)
	hayEfdo = 1;
if sum(flio)>0.0:
	pos &= (flio>0)
	hayFlio = 1;
if sum(fldo)>0.0:
	pos &= (fldo>0)
	hayFldo = 1;
#Comprobamos si hay datos
if hayFldo and (sum(pos)>0):
	#Voy a almacenar el sumatorio de las fluideces y el número de elementos del
	#vector como C=A*X+B, donde X será un factor igual a 1000000, que es
	#perfectamente válido en la práctica porque nunca va a haber un vector de
	#ese número de datos. El valor A es el sumatorio de las fluideces
	#multiplicadas por 100 y redondeadas para así quedarme con tres decimales y
	#el valor B es el número de elementos del vector
	#Convierto los datos a enteros incluyendo tres decimales
	fldo = np.round(fldo[pos]*1000)
	#El sumatorio de las fluideces lo multiplico por 1000000
	GC.result(np.sum(fldo)*1000000+len(fldo))
else:
	GC.result(0)
%% ;
		#Extraigo el sumatorio de los valores
		suma <- floor(dato/1000000);
		#Extraigo el número de datos
		n <- dato-suma*1000000;
	}
	#Media de los datos originales (recupero los decimales) y el número de datos
    value{ suma/n/1000; }
    count{ n; }
}
#######################################GPR######################################
{
	init
	{
		dato <-
%%python
import numpy as np
#Límites del balance
bmin = 20.0
bmax = 80.0
#Cargo los datos de trabajo
poto = np.asarray(GC.series(GC.SERIES_WATTS))
cado = np.asarray(GC.series(GC.SERIES_CAD))
lrbo = np.asarray(GC.series(GC.SERIES_LRBALANCE))
efio = np.asarray(GC.series(GC.SERIES_LTE))
efdo = np.asarray(GC.series(GC.SERIES_RTE))
flio = np.asarray(GC.series(GC.SERIES_LPS))
fldo = np.asarray(GC.series(GC.SERIES_RPS))
#Asigno valores 0.0 a los posibles valores -255 que se leen del fichero cuando
#alguna entrada del campo de balance está vacía
pos = lrbo<0.0
if sum(pos)>0:
	lrbo[pos] = 0.0
#Si no hay balance puede ser que el potenciómetro sea de una sola pierna, en
#cuyo caso asigno el valor 50%
if sum(lrbo)==0.0:
	lrbo = np.ones(len(poto))*50.0
#Buscamos los datos válidos (a partir de aquí la cadencia ya no se usa más)
pos = (cado>0)&(poto>0)&(lrbo>bmin)&(lrbo<bmax)
#No todos los potenciómetros almacenan efectividad y fluidez, por lo que hay que
#detectar si están presentes para luego usalos (si la serie no existe
#GoldenCheetah devuelve un vector de ceros al llamarla con GC.series)
hayEfio = 0;
hayEfdo = 0;
hayFlio = 0;
hayFldo = 0;
if sum(efio)>0.0:
	pos &= (efio>0)
	hayEfio = 1;
if sum(efdo)>0.0:
	pos &= (efdo>0)
	hayEfdo = 1;
if sum(flio)>0.0:
	pos &= (flio>0)
	hayFlio = 1;
if sum(fldo)>0.0:
	pos &= (fldo>0)
	hayFldo = 1;
#Comprobamos si no hay datos
if sum(pos)==0:
	#Valores ficticios para que no dé error el script
	pos = np.asarray([True])
	poto = np.asarray([0.1])
	lrbo = np.asarray([50.0])
	efio = np.asarray([100.0])
	efdo = np.asarray([100.0])
	flio = np.asarray([0.0])
	fldo = np.asarray([0.0])
#Nos quedamos sólo con los datos válidos
poto = poto[pos]
lrbo = lrbo[pos]
efio = efio[pos]
efdo = efdo[pos]
flio = flio[pos]
fldo = fldo[pos]
#Calculo las GPR y GPA
if hayEfio:
	#Calculo GPRL y GPAL
	gprlo = np.divide(np.multiply(poto,lrbo),efio)
	gpalo = gprlo-np.multiply(poto,np.divide(lrbo,100.0))
else:
	#Si no puedo calcularlas creo los vectores vacíos
	gprlo = np.zeros(len(poto))
	gpalo = np.zeros(len(poto))
if hayEfdo:
	#Calculo GPRR y GPAR
	gprro = np.divide(np.multiply(poto,100.0-lrbo),efdo)
	gparo = gprro-np.multiply(poto,1.0-np.divide(lrbo,100.0))
else:
	#Si no puedo calcularlas creo los vectores vacíos
	gprro = np.zeros(len(poto))
	gparo = np.zeros(len(poto))
#Debido a problemas en la lectura de datos los cálculos anteriores pueden
#devolver valores imposibles (miles de vatios, por ejemplo, para una sola pierna
#y tanto para valores generados como absorbidos). Un chequeo puede ser, por
#ejemplo, comprobar que ninguna GPR ni ninguna GPA pueden ser mayores que la
#potencia absoluta generada (realmente podría haber algún dato así, pero tiene
#una probabilidad bajísima, por no decir que lo más probable sería que fuera
#debido a un error del potenciómetro). También hay que chequear números muy
#pequeños negativos, tipo -2e-14, debido a problemas numéricos
maxPoto = max(poto)
pos = np.asarray([0.0])
pos1 = np.asarray([0.0])
pos2 = np.asarray([0.0])
if hayEfio:
	posValidasGprlo = (gprlo<maxPoto)&(gprlo>=0)
	posValidasGpalo = (gpalo<maxPoto)&(gpalo>=0)
	pos1 = posValidasGprlo&posValidasGpalo
if hayEfdo:
	posValidasGprro = (gprro<maxPoto)&(gprro>=0)
	posValidasGparo = (gparo<maxPoto)&(gparo>=0)
	pos2 = posValidasGprro&posValidasGparo
#Índices de los datos que valen
if sum(pos1)>0:
	pos = pos1
if sum(pos2)>0:
	if sum(pos1)>0:
		pos &= pos2
	else:
		pos = pos2
#Me quedo sólo con los datos válidos
if sum(pos)>0:
	gprlo = gprlo[pos]
	gprro = gprro[pos]
#Valor de la GPR
gpr = gprlo+gprro
#Voy a almacenar el sumatorio de las potencias y el número de elementos del
#vector como C=A*X+B, donde X será un factor igual a 1000000, que es
#perfectamente válido en la práctica porque nunca va a haber un vector de ese
#número de datos. El valor A es el sumatorio de las potencias multiplicadas por
#100 y redondeadas para así quedarme con tres decimales y el valor B es el
#número de elementos del vector
#Convierto los datos a enteros incluyendo tres decimales
gpr = np.round(gpr*1000)
#El sumatorio de las potencias lo multiplico por 1000000
GC.result(np.sum(gpr)*1000000+len(gpr))
%% ;
		#Extraigo el sumatorio de los valores
		suma <- floor(dato/1000000);
		#Extraigo el número de datos
		n <- dato-suma*1000000;
	}
	#Media de los datos originales (recupero los decimales) y el número de datos
    value{ suma/n/1000; }
    count{ n; }
}
#######################################GPA######################################
{
	init
	{
		dato <-
%%python
import numpy as np
#Límites del balance
bmin = 20.0
bmax = 80.0
#Cargo los datos de trabajo
poto = np.asarray(GC.series(GC.SERIES_WATTS))
cado = np.asarray(GC.series(GC.SERIES_CAD))
lrbo = np.asarray(GC.series(GC.SERIES_LRBALANCE))
efio = np.asarray(GC.series(GC.SERIES_LTE))
efdo = np.asarray(GC.series(GC.SERIES_RTE))
flio = np.asarray(GC.series(GC.SERIES_LPS))
fldo = np.asarray(GC.series(GC.SERIES_RPS))
#Asigno valores 0.0 a los posibles valores -255 que se leen del fichero cuando
#alguna entrada del campo de balance está vacía
pos = lrbo<0.0
if sum(pos)>0:
	lrbo[pos] = 0.0
#Si no hay balance puede ser que el potenciómetro sea de una sola pierna, en
#cuyo caso asigno el valor 50%
if sum(lrbo)==0.0:
	lrbo = np.ones(len(poto))*50.0
#Buscamos los datos válidos (a partir de aquí la cadencia ya no se usa más)
pos = (cado>0)&(poto>0)&(lrbo>bmin)&(lrbo<bmax)
#No todos los potenciómetros almacenan efectividad y fluidez, por lo que hay que
#detectar si están presentes para luego usalos (si la serie no existe
#GoldenCheetah devuelve un vector de ceros al llamarla con GC.series)
hayEfio = 0;
hayEfdo = 0;
hayFlio = 0;
hayFldo = 0;
if sum(efio)>0.0:
	pos &= (efio>0)
	hayEfio = 1;
if sum(efdo)>0.0:
	pos &= (efdo>0)
	hayEfdo = 1;
if sum(flio)>0.0:
	pos &= (flio>0)
	hayFlio = 1;
if sum(fldo)>0.0:
	pos &= (fldo>0)
	hayFldo = 1;
#Comprobamos si no hay datos
if sum(pos)==0:
	#Valores ficticios para que no dé error el script
	pos = np.asarray([True])
	poto = np.asarray([0.1])
	lrbo = np.asarray([50.0])
	efio = np.asarray([100.0])
	efdo = np.asarray([100.0])
	flio = np.asarray([0.0])
	fldo = np.asarray([0.0])
#Nos quedamos sólo con los datos válidos
poto = poto[pos]
lrbo = lrbo[pos]
efio = efio[pos]
efdo = efdo[pos]
flio = flio[pos]
fldo = fldo[pos]
#Calculo las GPR y GPA
if hayEfio:
	#Calculo GPRL y GPAL
	gprlo = np.divide(np.multiply(poto,lrbo),efio)
	gpalo = gprlo-np.multiply(poto,np.divide(lrbo,100.0))
else:
	#Si no puedo calcularlas creo los vectores vacíos
	gprlo = np.zeros(len(poto))
	gpalo = np.zeros(len(poto))
if hayEfdo:
	#Calculo GPRR y GPAR
	gprro = np.divide(np.multiply(poto,100.0-lrbo),efdo)
	gparo = gprro-np.multiply(poto,1.0-np.divide(lrbo,100.0))
else:
	#Si no puedo calcularlas creo los vectores vacíos
	gprro = np.zeros(len(poto))
	gparo = np.zeros(len(poto))
#Debido a problemas en la lectura de datos los cálculos anteriores pueden
#devolver valores imposibles (miles de vatios, por ejemplo, para una sola pierna
#y tanto para valores generados como absorbidos). Un chequeo puede ser, por
#ejemplo, comprobar que ninguna GPR ni ninguna GPA pueden ser mayores que la
#potencia absoluta generada (realmente podría haber algún dato así, pero tiene
#una probabilidad bajísima, por no decir que lo más probable sería que fuera
#debido a un error del potenciómetro). También hay que chequear números muy
#pequeños negativos, tipo -2e-14, debido a problemas numéricos
maxPoto = max(poto)
pos = np.asarray([0.0])
pos1 = np.asarray([0.0])
pos2 = np.asarray([0.0])
if hayEfio:
	posValidasGprlo = (gprlo<maxPoto)&(gprlo>=0)
	posValidasGpalo = (gpalo<maxPoto)&(gpalo>=0)
	pos1 = posValidasGprlo&posValidasGpalo
if hayEfdo:
	posValidasGprro = (gprro<maxPoto)&(gprro>=0)
	posValidasGparo = (gparo<maxPoto)&(gparo>=0)
	pos2 = posValidasGprro&posValidasGparo
#Índices de los datos que valen
if sum(pos1)>0:
	pos = pos1
if sum(pos2)>0:
	if sum(pos1)>0:
		pos &= pos2
	else:
		pos = pos2
#Me quedo sólo con los datos válidos
if sum(pos)>0:
	gpalo = gpalo[pos]
	gparo = gparo[pos]
#Valor de la GPA
gpa = gpalo+gparo
#Voy a almacenar el sumatorio de las potencias y el número de elementos del
#vector como C=A*X+B, donde X será un factor igual a 1000000, que es
#perfectamente válido en la práctica porque nunca va a haber un vector de ese
#número de datos. El valor A es el sumatorio de las potencias multiplicadas por
#100 y redondeadas para así quedarme con tres decimales y el valor B es el
#número de elementos del vector
#Convierto los datos a enteros incluyendo tres decimales
gpa = np.round(gpa*1000)
#El sumatorio de las potencias lo multiplico por 1000000
GC.result(np.sum(gpa)*1000000+len(gpa))
%% ;
		#Extraigo el sumatorio de los valores
		suma <- floor(dato/1000000);
		#Extraigo el número de datos
		n <- dato-suma*1000000;
	}
	#Media de los datos originales (recupero los decimales) y el número de datos
    value{ suma/n/1000; }
    count{ n; }
}































