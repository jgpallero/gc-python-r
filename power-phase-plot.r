## Power-phase-plot
#Este gráfico se acoge a la licencia BSD modificada de tres cláusulas
#Autor: José Luis García Pallero, jgpallero@gmail.com
#Cargo la actividad e información de los intervalos
act <- GC.activity()
laps <- GC.activity.intervals()
#Intervalo de validez para el balance
bmin <- 20
bmax <- 80
#Identificadores de existencia de datos
hayBal <- !(is.null(act$lrbalance))
hayLte <- !(is.null(act$lte))
hayRte <- !(is.null(act$rte))
hayLps <- !(is.null(act$lps))
hayRps <- !(is.null(act$rps))
#El balance lo extraemos siempre (o lo creamos a 50 si no existe)
if(hayBal)
{
    balance <- act$lrbalance
} else {
    balance <- rep(50,length(act$power))
}
#Condiciones generales de validez de los datos (dependiendo de los que haya)
cond <- (act$cadence>0)&(act$power>0)&(balance>bmin)&(balance<bmax)
if(hayLte)
{
    cond <- cond&(act$lte>0)
}
if(hayRte)
{
    cond <- cond&(act$rte>0)
}
if(hayLps)
{
    cond <- cond&(act$lps>0)
}
if(hayRps)
{
    cond <- cond&(act$rps>0)
}
#Condiciones de validez de tiempos y fases finales. Estas últimas se pueden
#chequear como >0, ya que al tener que estar en el entorno de 180, un valor
#menor que 0 indicará error
cond1 <- (act$seconds>0)
cond2 <- cond&(act$lppe>0)
cond3 <- cond&(act$rppe>0)
#Compruebo si hay seleccionado algún intervalo
lap_sel <- 1
if(any(laps$selected==TRUE))
{
    lap_sel <- which(laps$selected==TRUE)
}
#Selección de datos del intervalo
if(laps$start[lap_sel]!=laps$stop[lap_sel])
{
    cond1 <- (act$seconds>=laps$start[lap_sel])&
             (act$seconds<=laps$stop[lap_sel])
} else {
    cond1 <- act$seconds==laps$start[lap_sel]
}
int_name <- laps$name[lap_sel]
iso_power <- round(laps$IsoPower[lap_sel])
ifact <- laps$IF[lap_sel]*100.0
#Datos de trabajo
condl <- cond1&cond2
condr <- cond1&cond3
condp <- cond&cond1
#Algunos valores medios y otros datos
int_duration_s <- sum(condp)
avg_cad <- round(mean(act$cadence[condp]))
avg_power <- round(mean(act$power[condp]))
avg_lrb <- mean(balance[condp])
avg_lte <- mean(act$lte[condp])
avg_rte <- mean(act$rte[condp])
avg_lps <- mean(act$lps[condp])
avg_rps <- mean(act$rps[condp])
#Extraigo los datos de trabajo
lpps <- act$lppb[condl]
lppe <- act$lppe[condl]
rpps <- act$rppb[condr]
rppe <- act$rppe[condr]
lppps <- act$lpppb[condl]
lpppe <- act$lpppe[condl]
rppps <- act$rpppb[condr]
rpppe <- act$rpppe[condr]
#Pongo el inicio de fase en el dominio (-180,180]
domLpps <- lpps>180
domRpps <- rpps>180
domLppps <- lppps>180
domRppps <- rppps>180
lpps[domLpps] <- lpps[domLpps]-360
rpps[domRpps] <- rpps[domRpps]-360
lppps[domLppps] <- lppps[domLppps]-360
rppps[domRppps] <- rppps[domRppps]-360
#Dinámicas de pedaleo
alpps <- mean(lpps)
alppe <- mean(lppe)
arpps <- mean(rpps)
arppe <- mean(rppe)
alppps <- mean(lppps)
alpppe <- mean(lpppe)
arppps <- mean(rppps)
arpppe <- mean(rpppe)
#Busco valores NA, que quiere decir que el dato no está disponible
if(any(is.na(c(alpps,alppe,alppps,alpppe))))
{
    alpps <- 0
    alppe <- 0
    alppps <- 0
    alpppe <- 0
}
if(any(is.na(c(arpps,arppe,arppps,arpppe))))
{
    arpps <- 0
    arppe <- 0
    arppps <- 0
    arpppe <- 0
}
#Calculo GPR y GPA
potl <- act$power[condp]
potr <- act$power[condp]
lrbl <- balance[condp]
lrbr <- balance[condp]
lte <- act$lte[condp]
rte <- act$rte[condp]
gprl <- (potl*lrbl)/lte
gprr <- (potr*(100-lrbr))/rte
gpal <- gprl-potl*lrbl/100
gpar <- gprr-potr*(1-lrbr/100)
gprl <- mean(gprl)
gprr <- mean(gprr)
gpal <- mean(gpal)
gpar <- mean(gpar)
#Tiempo válido
hourst = int_duration_s/3600
hours <- floor(hourst)
minutest <- (hourst-hours)*60
minutes <- floor(minutest)
seconds <- round((minutest-minutes)*60)
int_duration <- sprintf("%dh %dm %ds",hours,minutes,seconds)
#Tiempo total
int_duration_s_t = sum(cond1)
hourstt = int_duration_s_t/3600
hourst <- floor(hourstt)
minutestt <- (hourstt-hourst)*60
minutest <- floor(minutestt)
secondst <- round((minutestt-minutest)*60)
int_duration_t <- sprintf("%dh %dm %ds",hourst,minutest,secondst)
#Construcción de las cadenas de texto
title_string <- sprintf("%s\nTotal time: %s\nValid data (P>0): %s\nValid data (P>0): %.1f %%\nCadence: %.0f rpm\nPower: %.0f W\nNP: %.0f W\nIF: %.1f %%",
                        int_name,int_duration_t,int_duration,int_duration_s/int_duration_s_t*100.0,avg_cad,avg_power,iso_power,ifact)
title_left <- sprintf("%20s %.1f %%\n%20s %.1f %%\n%15s %.1f %%\n%21s %3.0f W\n%21s %3.0f W",
                      "Balance:",avg_lrb,"Torque eff.:",avg_lte,"Pedal smooth.:",avg_lps,"GPR:",gprl,"GPA:",gpal)
title_right <- sprintf("%20s %.1f %%\n%20s %.1f %%\n%15s %.1f %%\n%21s %3.0f W\n%21s %3.0f W",
                      "Balance:",100-avg_lrb,"Torque eff.:",avg_rte,"Pedal smooth.:",avg_rps,"GPR:",gprr,"GPA:",gpar)
################################################################################
## Function definition
# function to create a circle
circle <- function(center=c(0,0),radius=1,npoints=360)
{
    r = radius
    tt = seq(0,2*pi,length=npoints)
    xx = center[1]+r*cos(tt)
    yy = center[2]+r*sin(tt)
    return(data.frame(x=xx,y=yy))
}

# function to get slices
slice2xy <- function(t,rad)
{
    t2p = -t*pi/180+pi/2
    list(x=rad*cos(t2p),y=rad*sin(t2p))
}

# function to draw slices
drawSlice <- function(Pstart=0,Pend=180,rad=1.0,Pcolor="red")
{
    S = slice2xy(seq.int(Pstart,Pend,length.out=30),rad)
    polygon(c(S$x,0),c(S$y,0),border=Pcolor,col=Pcolor,lty=NULL)
}

# function to get major and minor tick marks
ticks <- function(center=c(0,0),from=0,to=2*pi,radius=0.9,npoints=5)
{
    r = radius
    tt = seq(from,to,length=npoints)
    xx = center[1]+r*cos(tt)
    yy = center[2]+r*sin(tt)
    return(data.frame(x=xx,y=yy))
}

# function to label start and end points
labelPosition <- function(degrees=0,radius=0.9,color="green")
{
    v0 = -degrees*pi/180+pi/2
    Pos0 = list(x=radius*cos(v0),y=radius*sin(v0))
    text(Pos0,labels=degrees,col=color,cex=1.5)
}

# function to plot radial gauge chart
plotRGC <- function (Pstart=350,Pend=220,PPstart=61,PPend=120,Pedal="Left leg")
{
    #using round() instead of floor()
    Pstart = round(Pstart)
    Pend = round(Pend)
    PPstart = round(PPstart)
    PPend = round(PPend)

    # external circle (this will be used for the black border)
    border_cir = circle(c(0,0),radius=1,npoints=360)


    # coordinates of major ticks (will be plotted as arrows)
    major_ticks_out = ticks(c(0,0),from=0,to=3*pi/2,radius=1.0,4)
    major_ticks_in = ticks(c(0,0),from=0,to=3*pi/2,radius=0.8,4)

    # open plot
    plot(border_cir$x,border_cir$y,type="n",asp=1,axes=FALSE,
         xlim=c(-1.05,1.05),ylim=c(-1.05,1.05),xlab="",ylab="")

    # draw slices
    drawSlice(Pstart,Pend,rad=1.0,Pcolor="orange")
    drawSlice(Pstart,Pend,rad=0.8,Pcolor="white")
    drawSlice(PPstart,PPend,rad=0.8,Pcolor="red")
    drawSlice(Pstart,Pend,rad=0.6,Pcolor="white")

    # add external border
    lines(border_cir$x,border_cir$y,col="black",lwd=4)

    # add major ticks
    arrows(x0=major_ticks_out$x,y0=major_ticks_out$y,
           x1=major_ticks_in$x,y1=major_ticks_in$y,length=0,lwd=2.5)

    # add label of pedal
    text(0,1.2,Pedal,cex=2,font=2)
    text(0.1,0.0,"Peak",cex=1.5,col="red")

    # Postion labels
    labelPosition(Pstart%%360,0.7,"orange")
    labelPosition(Pend,0.7,"orange")
    labelPosition(PPstart%%360,0.45,"red")
    labelPosition(PPend,0.45,"red")
}
################################################################################
#Impresión de resultados
par(mfrow=c(1,2),mar=c(0,0,0,0))
plotRGC(alpps,alppe,alppps,alpppe,Pedal="Left leg")
plotRGC(arpps,arppe,arppps,arpppe,Pedal="Right leg")
#Impresión de otra información
par(lheight=1.25)
text(-1.15,0.96,title_string,cex=1.5,font=2,col="white",xpd=NA)
text(-2.9,0.04,title_left,cex=1.5,font=2,col="black",xpd=NA)
text(-0.63,0.04,title_right,cex=1.5,font=2,col="black",xpd=NA)
################################################################################
#Copyright (c) 2019-2022, José Luis García Pallero, jgpallero@gmail.com
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#   Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#   Neither the name of copyright holders nor the names of its contributors may
#   be used to endorse or promote products derived from this software without
#   specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
