#This graphic is published under the modified 3-clause BSD license
#Author: José Luis García Pallero, jgpallero@gmail.com
import os
import numpy as np
import datetime as dtm
from math import ceil,sqrt
import scipy
from scipy.interpolate import interp1d
from scipy.signal import medfilt
import plotly
from plotly.graph_objs import Scatter,Layout,Data
from plotly import subplots
from pathlib import Path

#Dato a dibujar en cada panel: "balance", "gpra", "efectividad", "fluidez",""
#¡¡SI SÓLO SE QUIERE UN DATO A DIBUJAR, PONER UNA COMA TRAS ÉL ES OBLIGATORIO!!
#En los datos de GPR/GPA puede parecer que hay inconsistencias entre los valores
#absolutos de potencia medios y los balances medios, pero no es así, ya que los
#valores absolutos son la media de los datos de potencia (total, GPR y GPA) y
#los balances son las medias de los balances calculados con cada dato, que no
#tienen por qué coincidir con un balance medio calculado con los valores medios
#de potencia
paneles = ("balance","gpra","efectividad","fluidez")
#Dibuja tiempo en zonas
dtz = 1
#Dibuja los datos brutos en el gráfico del balance
ddb = 1
#Tamaño de grupo de valores de potencia (en vatios), número mínimo de
#observaciones en cada grupo para considerarlo y tamaño de filtro
tg = 5
nm = 5
tv = 3
#Balances mínimo y máximo para tener un dato en cuenta en los cálculos
bmin = 20.0
bmax = 80.0
#Balances de GPR mínimo y máximo para el eje Y del gráfico correspondiente
bmingpr = bmin
bmaxgpr = bmax
#Efectividades mínima y máxima para el eje Y del gráfico correspondiente
emin = 40.0
emax = 100.0
#Fluideces mínima y máxima para el eje Y del gráfico correspondiente
fmin = 10.0
fmax = 35.0
#Potencia a partir de la cual representar en el dibujo
pmin = 50.0
#Potencia hasta la que dibujar: 0/1->máxima absoluta/máxima con media
pmaxd = 0
#Nombre de los ficheros a crear
nomfichhtml = str(Path.home())+os.path.sep+"balance-individual.html"
nomfichtxt = str(Path.home())+os.path.sep+"balance-individual-datos.txt"
################################################################################
#Número de paneles a manejar
panelest = []
npan = 0
for i in range(len(paneles)):
	if paneles[i]!="":
		panelest.append(paneles[i])
		npan = npan+1
##Los títulos de los dibujos no se van a usar, pero pongo aquí su creación por
##si es necesario acordarse en el futuro
#titulos_sp = []
#for i in range(npan):
	#if panelest[i]=="balance":
		#titulos_sp.append("Balance (pierna izquierda)")
	#elif panelest[i]=="gpra":
		#titulos_sp.append("Balance potencia generada/absorbida")
	#elif panelest[i]=="efectividad":
		#titulos_sp.append("Efectividad del pedaleo")
	#elif panelest[i]=="fluidez":
		#titulos_sp.append("Fluidez del pedaleo")
	#else:
		#titulos_sp.append("Ritmo cardíaco")
#titulos_sp = tuple(titulos_sp)
################################################################################
#Información de la actividad y zonas de potencia
act = GC.activityMetrics()
zonas = GC.athleteZones(act["date"])
try:
    pb = zonas["sport"].index("Bike")
except:
    pb = zonas["sport"].index("bike")
zonas = zonas["zoneslow"][pb]
#Límites de las zonas y vectores de datos en zonas
nz = len(zonas)
lz = np.zeros((nz,2))
for i in range(nz-1):
	lz[i][0] = zonas[i]
	lz[i][1] = zonas[i+1]
lz[nz-1][0] = zonas[nz-1]
lz[nz-1][1] = 10000
tz = np.zeros(nz)
pz = np.zeros(nz)
lrbz = np.zeros(nz)
efiz = np.zeros(nz)
efdz = np.zeros(nz)
fliz = np.zeros(nz)
fldz = np.zeros(nz)
gprlz = np.zeros(nz)
gprrz = np.zeros(nz)
gpalz = np.zeros(nz)
gparz = np.zeros(nz)
gprbz = np.zeros(nz)
gpabz = np.zeros(nz)
#Tiempos
secs = np.asarray(GC.series(GC.SERIES_SECS))
#Vueltas
laps = GC.activityIntervals()
#Posiciones de los datos del intervalo seleccionado
pos = secs<0
for i in range(len(laps["selected"])):
	if laps["selected"][i]:
		#Posiciones de los datos
		if laps["start"][i]!=laps["stop"][i]:
			pos |= (secs>=laps["start"][i])&(secs<laps["stop"][i])
		else:
			#Vuelta de un único segundo
			pos |= secs==laps["start"][i]
		#Título de la actividad
		titulo = laps["name"][i]
		break
	elif i==(len(laps["selected"])-1):
		#Saco los datos de la actividad completa
		pos |= (secs>=laps["start"][0])&(secs<laps["stop"][0])
		titulo = laps["name"][0]
#Carga de datos de trabajo y extracción del intervalo seleccionado
poto = np.asarray(GC.series(GC.SERIES_WATTS))[pos]
cado = np.asarray(GC.series(GC.SERIES_CAD))[pos]
lrbo = np.asarray(GC.series(GC.SERIES_LRBALANCE))[pos]
efio = np.asarray(GC.series(GC.SERIES_LTE))[pos]
efdo = np.asarray(GC.series(GC.SERIES_RTE))[pos]
flio = np.asarray(GC.series(GC.SERIES_LPS))[pos]
fldo = np.asarray(GC.series(GC.SERIES_RPS))[pos]
#Asigno valores 0.0 a los posibles valores -255 que se leen del fichero cuando
#alguna entrada del campo de balance está vacía
pos = lrbo<0.0
if sum(pos)>0:
	lrbo[pos] = 0.0
#Si no hay balance puede ser que el potenciómetro sea de una sola pierna, en
#cuyo caso asigno el valor 50%
if sum(lrbo)==0.0:
	lrbo = np.ones(len(poto))*50.0
#Buscamos los datos válidos (a partir de aquí la cadencia ya no se usa más)
pos = (cado>0)&(poto>0)&(lrbo>bmin)&(lrbo<bmax)
#No todos los potenciómetros almacenan efectividad y fluidez, por lo que hay que
#detectar si están presentes para luego usalos (si la serie no existe
#GoldenCheetah devuelve un vector de ceros al llamarla con GC.series)
hayEfio = 0;
hayEfdo = 0;
hayFlio = 0;
hayFldo = 0;
if sum(efio)>0.0:
	pos &= (efio>0)
	hayEfio = 1;
if sum(efdo)>0.0:
	pos &= (efdo>0)
	hayEfdo = 1;
if sum(flio)>0.0:
	pos &= (flio>0)
	hayFlio = 1;
if sum(fldo)>0.0:
	pos &= (fldo>0)
	hayFldo = 1;
#Comprobamos si no hay datos
if sum(pos)==0:
	#Valores ficticios para que no dé error el script
	pos = np.asarray([True])
	poto = np.asarray([0.1])
	lrbo = np.asarray([50.0])
	efio = np.asarray([100.0])
	efdo = np.asarray([100.0])
	flio = np.asarray([0.0])
	fldo = np.asarray([0.0])
#Nos quedamos sólo con los datos válidos
poto = poto[pos]
lrbo = lrbo[pos]
efio = efio[pos]
efdo = efdo[pos]
flio = flio[pos]
fldo = fldo[pos]
#Calculo las GPR y GPA
if hayEfio:
	#Calculo GPRL y GPAL
	gprlo = np.divide(np.multiply(poto,lrbo),efio)
	gpalo = gprlo-np.multiply(poto,np.divide(lrbo,100.0))
else:
	#Si no puedo calcularlas creo los vectores vacíos
	gprlo = np.zeros(len(poto))
	gpalo = np.zeros(len(poto))
if hayEfdo:
	#Calculo GPRR y GPAR
	gprro = np.divide(np.multiply(poto,100.0-lrbo),efdo)
	gparo = gprro-np.multiply(poto,1.0-np.divide(lrbo,100.0))
else:
	#Si no puedo calcularlas creo los vectores vacíos
	gprro = np.zeros(len(poto))
	gparo = np.zeros(len(poto))
#Debido a problemas en la lectura de datos los cálculos anteriores pueden
#devolver valores imposibles (miles de vatios, por ejemplo, para una sola pierna
#y tanto para valores generados como absorbidos). Un chequeo puede ser, por
#ejemplo, comprobar que ninguna GPR ni ninguna GPA pueden ser mayores que la
#potencia absoluta generada (realmente podría haber algún dato así, pero tiene
#una probabilidad bajísima, por no decir que lo más probable sería que fuera
#debido a un error del potenciómetro). También hay que chequear números muy
#pequeños negativos, tipo -2e-14, debido a problemas numéricos
maxPoto = max(poto)
pos = np.asarray([0.0])
pos1 = np.asarray([0.0])
pos2 = np.asarray([0.0])
if hayEfio:
	posValidasGprlo = (gprlo<maxPoto)&(gprlo>=0)
	posValidasGpalo = (gpalo<maxPoto)&(gpalo>=0)
	pos1 = posValidasGprlo&posValidasGpalo
if hayEfdo:
	posValidasGprro = (gprro<maxPoto)&(gprro>=0)
	posValidasGparo = (gparo<maxPoto)&(gparo>=0)
	pos2 = posValidasGprro&posValidasGparo
#Índices de los datos que valen
if sum(pos1)>0:
	pos = pos1
if sum(pos2)>0:
	if sum(pos1)>0:
		pos &= pos2
	else:
		pos = pos2
#Me quedo sólo con los datos válidos
if sum(pos)>0:
	poto = poto[pos]
	lrbo = lrbo[pos]
	efio = efio[pos]
	efdo = efdo[pos]
	flio = flio[pos]
	fldo = fldo[pos]
	gprlo = gprlo[pos]
	gprro = gprro[pos]
	gpalo = gpalo[pos]
	gparo = gparo[pos]
#Calculo los balances de potencia generada y absorbida, si ha lugar
if hayEfio and hayEfdo:
	#Si en algún momento la potencia absorbida es 0 lrbao almacena NaN, pero en
	#realidad es el 50%. Desactivo el warning por posible división entre 0 sólo
	#para estas operaciones
	with np.errstate(divide='ignore',invalid='ignore'):
		#Balance de potencia generada
		lrbro = np.nan_to_num(np.divide(gprlo,gprlo+gprro),nan=0.5)*100.0
		#Balance de potencia absorbida
		lrbao = np.nan_to_num(np.divide(gpalo,gpalo+gparo),nan=0.5)*100.0
elif hayEfio or hayEfdo:
	#Si estamos con potenciómetro de una pata asigno el 50% a los balances
	lrbro = np.ones(len(poto))*50.0
	lrbao = np.ones(len(poto))*50.0
else:
	#Si no hay efectividad, asigno cero a estos vectores
	lrbro = np.zeros(len(poto))
	lrbao = np.zeros(len(poto))
#Tiempo de datos válidos de la actividad
tact = str(dtm.timedelta(seconds=len(poto)))
#Valores medios como cadena de texto
potmed = str(round(np.mean(poto)))
lrblmed = str(round(np.mean(lrbo),1))
lrbrmed = str(round(np.mean(100.0-lrbo),1))
efimed = str(round(np.mean(efio),1))
efdmed = str(round(np.mean(efdo),1))
flimed = str(round(np.mean(flio),1))
fldmed = str(round(np.mean(fldo),1))
gprlmed = str(round(np.mean(gprlo)))
gprrmed = str(round(np.mean(gprro)))
gpalmed = str(round(np.mean(gpalo)))
gparmed = str(round(np.mean(gparo)))
if hayEfio and hayEfdo:
	balgprlmed = str(round(np.mean(lrbro),1))
	balgprrmed = str(round(np.mean(100.0-lrbro),1))
	balgpalmed = str(round(np.mean(lrbao),1))
	balgparmed = str(round(np.mean(100.0-lrbao),1))
elif hayEfio or hayEfdo:
	balgprlmed = str(50.0)
	balgprrmed = str(50.0)
	balgpalmed = str(50.0)
	balgparmed = str(50.0)
else:
	balgprlmed = str(0.0)
	balgprrmed = str(0.0)
	balgpalmed = str(0.0)
	balgparmed = str(0.0)
#Actualización del título
titulo = "<b>"+titulo+".</b> Datos para P>0. "+ \
         "Tiempo T=<b>"+tact+"</b>, P=<b>"+potmed+"W</b>, "+ \
         "Balance(L/R)=<b>"+lrblmed+"%/"+lrbrmed+"%</b>, "+ \
         "Efectividad(L/R)=<b>"+efimed+"%/"+efdmed+"%</b>, "+ \
         "Fluidez(L/R)=<b>"+flimed+"%/"+fldmed+"%</b><br>"+ \
         "GPRL=<b>"+gprlmed+"W</b>, GPRR=<b>"+gprrmed+"W</b>, "+ \
         "GPAL=<b>"+gpalmed+"W</b>, GPAR=<b>"+gparmed+"W</b>, "+ \
         "Balance GPR(L/R)=<b>"+balgprlmed+"%/"+balgprrmed+"%</b>, "+ \
         "Balance GPA(L/R)=<b>"+balgpalmed+"%/"+balgparmed+"%</b>"
#Calculamos los datos relativos a las zonas
ns = len(poto)
for i in range(nz):
	pos = (poto>=lz[i][0])&(poto<lz[i][1])
	nsp = len(poto[pos])
	if nsp>0:
		tz[i] = nsp/ns*100
		pz[i] = np.nan_to_num(np.mean(poto[pos]),nan=0.0)
		lrbz[i] = np.nan_to_num(np.mean(lrbo[pos]),nan=50.0)
		efiz[i] = np.nan_to_num(np.mean(efio[pos]),nan=0.0)
		efdz[i] = np.nan_to_num(np.mean(efdo[pos]),nan=0.0)
		fliz[i] = np.nan_to_num(np.mean(flio[pos]),nan=0.0)
		fldz[i] = np.nan_to_num(np.mean(fldo[pos]),nan=0.0)
		if hayEfio:
			gprlz[i] = np.nan_to_num(np.mean(gprlo[pos]),nan=0.0)
			gpalz[i] = np.nan_to_num(np.mean(gpalo[pos]),nan=0.0)
			gprbz[i] = np.nan_to_num(np.mean(lrbro[pos]),nan=50.0)
			gpabz[i] = np.nan_to_num(np.mean(lrbao[pos]),nan=50.0)
		if hayEfdo:
			gprrz[i] = np.nan_to_num(np.mean(gprro[pos]),nan=0.0)
			gparz[i] = np.nan_to_num(np.mean(gparo[pos]),nan=0.0)
			gprbz[i] = np.nan_to_num(np.mean(lrbro[pos]),nan=50.0)
			gpabz[i] = np.nan_to_num(np.mean(lrbao[pos]),nan=50.0)
#Número de grupos de valores de potencia
ng = int(ceil(poto.max()/tg))
#Valores de potencia correspondientes al centro de cada grupo
pini = (tg+1.0)/2.0
pfin = pini+tg*(ng-1)
#Creo los vectores de trabajo del tamaño calculado
pot = np.linspace(pini,pfin,num=ng)
nel = np.zeros(ng)
lrb = np.zeros(ng)
efi = np.zeros(ng)
efd = np.zeros(ng)
fli = np.zeros(ng)
fld = np.zeros(ng)
gprl = np.zeros(ng)
gprr = np.zeros(ng)
gpal = np.zeros(ng)
gpar = np.zeros(ng)
lrbr = np.zeros(ng)
lrba = np.zeros(ng)
#Recorro los datos de potencia para el cálculo de la media
for i in range(poto.size):
	#Índice en los vectores de trabajo
	ind = int(ceil(poto[i]/tg)-1)
	#Sumo y actualizo la media
	lrb[ind] = lrb[ind]*nel[ind]+lrbo[i]
	efi[ind] = efi[ind]*nel[ind]+efio[i]
	efd[ind] = efd[ind]*nel[ind]+efdo[i]
	fli[ind] = fli[ind]*nel[ind]+flio[i]
	fld[ind] = fld[ind]*nel[ind]+fldo[i]
	gprl[ind] = gprl[ind]*nel[ind]+gprlo[i]
	gprr[ind] = gprr[ind]*nel[ind]+gprro[i]
	gpal[ind] = gpal[ind]*nel[ind]+gpalo[i]
	gpar[ind] = gpar[ind]*nel[ind]+gparo[i]
	lrbr[ind] = lrbr[ind]*nel[ind]+lrbro[i]
	lrba[ind] = lrba[ind]*nel[ind]+lrbao[i]
	nel[ind] = nel[ind]+1
	lrb[ind] = lrb[ind]/nel[ind]
	efi[ind] = efi[ind]/nel[ind]
	efd[ind] = efd[ind]/nel[ind]
	fli[ind] = fli[ind]/nel[ind]
	fld[ind] = fld[ind]/nel[ind]
	gprl[ind] = gprl[ind]/nel[ind]
	gprr[ind] = gprr[ind]/nel[ind]
	gpal[ind] = gpal[ind]/nel[ind]
	gpar[ind] = gpar[ind]/nel[ind]
	lrbr[ind] = lrbr[ind]/nel[ind]
	lrba[ind] = lrba[ind]/nel[ind]
#Elimino las potencias sin dato y sin número mínimo de observaciones
pos = nel>=nm
potn0 = pot[pos]
neln0 = nel[pos]
lrbn0 = lrb[pos]
efin0 = efi[pos]
efdn0 = efd[pos]
flin0 = fli[pos]
fldn0 = fld[pos]
gprln0 = gprl[pos]
gprrn0 = gprr[pos]
gpaln0 = gpal[pos]
gparn0 = gpar[pos]
lrbrn0 = lrbr[pos]
lrban0 = lrba[pos]
#Compruebo si hay que interpolar
if (potn0.size>1) and (potn0.size<pot.size):
	#Calculo las funciones de interpolación
	filrb = interp1d(potn0,lrbn0,fill_value="extrapolate")
	fiefi = interp1d(potn0,efin0,fill_value="extrapolate")
	fiefd = interp1d(potn0,efdn0,fill_value="extrapolate")
	fifli = interp1d(potn0,flin0,fill_value="extrapolate")
	fifld = interp1d(potn0,fldn0,fill_value="extrapolate")
	figprl = interp1d(potn0,gprln0,fill_value="extrapolate")
	figprr = interp1d(potn0,gprrn0,fill_value="extrapolate")
	figpal = interp1d(potn0,gpaln0,fill_value="extrapolate")
	figpar = interp1d(potn0,gparn0,fill_value="extrapolate")
	filrbr = interp1d(potn0,lrbrn0,fill_value="extrapolate")
	filrba = interp1d(potn0,lrban0,fill_value="extrapolate")
	#Creo el nuevo vector de datos e interpolo
	pini = potn0.min()
	pfin = potn0.max()
	ng = int((pfin-pini)/tg+1)
	pot = np.linspace(pini,pfin,num=ng)
	lrb = filrb(pot)
	efi = fiefi(pot)
	efd = fiefd(pot)
	fli = fifli(pot)
	fld = fifld(pot)
	gprl = figprl(pot)
	gprr = figprr(pot)
	gpal = figpal(pot)
	gpar = figpar(pot)
	lrbr = filrbr(pot)
	lrba = filrba(pot)
#Filtro los datos, si ha lugar
if (potn0.size>1) and (tv>1):
	lrb = medfilt(lrb,kernel_size=tv)
	efi = medfilt(efi,kernel_size=tv)
	efd = medfilt(efd,kernel_size=tv)
	fli = medfilt(fli,kernel_size=tv)
	fld = medfilt(fld,kernel_size=tv)
	gprl = medfilt(gprl,kernel_size=tv)
	gprr = medfilt(gprr,kernel_size=tv)
	gpal = medfilt(gpal,kernel_size=tv)
	gpar = medfilt(gpar,kernel_size=tv)
	lrbr = medfilt(lrbr,kernel_size=tv)
	lrba = medfilt(lrba,kernel_size=tv)
#Para la escritura de datos en fichero voy a ordenar por GPR total
gpro = np.round(gprlo+gprro).astype(int)
gprom = min(gpro)
gproM = max(gpro)
fil = gproM-gprom+1
#Abro el fichero de trabajo
idf = open(nomfichtxt,"w")
idf.write("%Los datos de este fichero, al contrario que los del dibujo, no \
están filtrados")
idf.write(chr(10))
idf.write("%GPRL+GPRR puede no ser igual a GPR por problemas de redondeo")
idf.write(chr(10))
idf.write("%GPR   Nelem GPRL GPRR GPAL GPAR EfectL EfectR SuavL SuavR \
BalanceL  Pot")
idf.write(chr(10))
#Recorro el número de valores de potencia
for i in range(fil):
    #Valor de GPR de trabajo
    gprt = gprom+i
    #Busco las posiciones con los valores de GPR
    pos = np.where(gpro==gprt)
    #Si hay datos con este valor, los imprimo
    if np.any(pos):
        idf.write("%4d %7d %4d %4d %4d %4d %6.1f %6.1f %5.1f %5.1f %8.1f %4d" \
                  % (gprt,len(pos[0]),np.mean(gprlo[pos]),np.mean(gprro[pos]),\
                     np.mean(gpalo[pos]),np.mean(gparo[pos]),\
                     np.mean(efio[pos]),np.mean(efdo[pos]),np.mean(flio[pos]),\
                     np.mean(fldo[pos]),np.mean(lrbo[pos]),np.mean(poto[pos])))
        idf.write(chr(10))
#Cierro el fichero de trabajo
idf.close()
#Potencia máxima para el eje X
if pmaxd==0:
	pmaxd = poto.max()
else:
	pmaxd = pfin
#Identificadores de ejes de subplots
for i in range(npan):
	xrefx = "x"+str(i+1)
	yrefx = "y"+str(i+1)
	if panelest[i]=="balance":
		xrefb = xrefx
		yrefb = yrefx
	elif panelest[i]=="gpra":
		xrefg = xrefx
		yrefg = yrefx
	elif panelest[i]=="efectividad":
		xrefe = xrefx
		yrefe = yrefx
	elif panelest[i]=="fluidez":
		xreff = xrefx
		yreff = yrefx
#Marcadores de zonas
lineavb = []
lineave = []
lineavf = []
lineavg = []
textozb = []
textozp = []
textoze = []
textozf = []
textozg = []
for i in range(nz):
	lineavb.append(Scatter(x=[lz[i][0],lz[i][0]],y=[bmin,bmax],
						   hoverinfo="none",line=dict(color="black",width=0.75),
						   showlegend=False))
	lineavg.append(Scatter(x=[lz[i][0],lz[i][0]],y=[bmingpr,bmaxgpr],
						   hoverinfo="none",line=dict(color="black",width=0.75),
						   showlegend=False))
	lineave.append(Scatter(x=[lz[i][0],lz[i][0]],y=[emin,emax],
						   hoverinfo="none",line=dict(color="black",width=0.75),
						   showlegend=False))
	lineavf.append(Scatter(x=[lz[i][0],lz[i][0]],y=[fmin,fmax],
						   hoverinfo="none",line=dict(color="black",width=0.75),
						   showlegend=False))
	textozb.append(dict(x=lz[i][0],y=bmax,xref=xrefb,yref=yrefb,
				   align="left",xanchor="left",yanchor="top",showarrow=False,
				   text="Z%d=%.1f%%T<br>t=%s"
				   %(i+1,tz[i],dtm.timedelta(seconds=tz[i]*ns/100))))
	textozb.append(dict(x=lz[i][0],y=bmin,xref=xrefb,yref=yrefb,
				   align="left",xanchor="left",yanchor="bottom",showarrow=False,
				   text="P=%dW<br>B(%%)=%.1f/%.1f"
				   %(pz[i],lrbz[i],100.0-lrbz[i])))
	textozg.append(dict(x=lz[i][0],y=bmaxgpr,xref=xrefg,yref=yrefg,
				   align="left",xanchor="left",yanchor="top",showarrow=False,
				   text="GPR(W)=%d/%d<br>B(%%)=%.1f/%.1f"
				   %(gprlz[i],gprrz[i],gprbz[i],100.0-gprbz[i])))
	textozg.append(dict(x=lz[i][0],y=bmingpr,xref=xrefg,yref=yrefg,
				   align="left",xanchor="left",yanchor="bottom",showarrow=False,
				   text="GPA(W)=%d/%d<br>B(%%)=%.1f/%.1f"
				   %(gpalz[i],gparz[i],gpabz[i],100.0-gpabz[i])))
	textoze.append(dict(x=lz[i][0],y=emin,xref=xrefe,yref=yrefe,
				   align="left",xanchor="left",yanchor="bottom",showarrow=False,
				   text="E(%%)=%.1f/%.1f"%(efiz[i],efdz[i])))
	textozf.append(dict(x=lz[i][0],y=fmin,xref=xreff,yref=yreff,
				   xanchor="left",yanchor="bottom",showarrow=False,
				   text="F(%%)=%.1f/%.1f"%(fliz[i],fldz[i])))
#Apariencia de los datos
if ddb!=0:
	ptos = Scatter(x=poto,y=lrbo,mode="markers",
				   marker=dict(color="rgb(150,150,255)",size=2),hoverinfo="none",
			       name="Datos brutos")
mediab = Scatter(x=pot,y=lrb,line=dict(color="rgb(255,0,0)",width=5),
				 name="Pierna izquierda")
mediaei = Scatter(x=pot,y=efi,line=dict(color="rgb(255,0,0)",width=5),
				  name="Pierna izquierda")
mediaed = Scatter(x=pot,y=efd,line=dict(color="rgb(0,200,0)",width=5),
				  name="Pierna derecha")
mediafi = Scatter(x=pot,y=fli,line=dict(color="rgb(255,0,0)",width=5),
				  name="Pierna izquierda")
mediafd = Scatter(x=pot,y=fld,line=dict(color="rgb(0,200,0)",width=5),
				  name="Pierna derecha")
mediagpr = Scatter(x=pot,y=lrbr,line=dict(color="rgb(255,0,0)",width=5),
				  name="GPR izquierda")
mediagpa = Scatter(x=pot,y=lrba,line=dict(color="rgb(0,200,200)",width=5),
				  name="GPA izquierda")
#Creo la estructura general del dibujo
#fig = subplots.make_subplots(rows=npan,cols=1,subplot_titles=titulos_sp,
				             #vertical_spacing = 0.03)
fig = subplots.make_subplots(rows=npan,cols=1,vertical_spacing = 0.03)
fig["layout"].update(title=titulo,plot_bgcolor="rgba(240,240,240,240)",
                     hovermode="x")
#Modifico los márgenes (top, bottom, left, right, en píxeles)
fig["layout"].update(margin=dict(t=100,b=10,l=1,r=1))
for i in range(npan):
	nxaxis = "xaxis"+str(i+1)
	nyaxis = "yaxis"+str(i+1)
	if panelest[i]=="balance":
		if ddb!=0:
			fig.append_trace(ptos,i+1,1)
		fig.append_trace(mediab,i+1,1)
		for j in range(nz):
			fig.append_trace(lineavb[j],i+1,1)
		fig["layout"]["annotations"] += tuple(textozb)
		fig["layout"][nxaxis].update(range=[pmin,pmaxd])
		fig["layout"][nyaxis].update(title="Balance (%)",range=[bmin,bmax])
	elif panelest[i]=="gpra":
		fig.append_trace(mediagpr,i+1,1)
		fig.append_trace(mediagpa,i+1,1)
		for j in range(nz):
			fig.append_trace(lineavg[j],i+1,1)
		fig["layout"]["annotations"] += tuple(textozg)
		fig["layout"][nxaxis].update(range=[pmin,pmaxd])
		fig["layout"][nyaxis].update(title="GPR/GPA (%)",range=[bmingpr,bmaxgpr])
	elif panelest[i]=="efectividad":
		fig.append_trace(mediaei,i+1,1)
		fig.append_trace(mediaed,i+1,1)
		for j in range(nz):
			fig.append_trace(lineave[j],i+1,1)
		fig["layout"]["annotations"] += tuple(textoze)
		fig["layout"][nxaxis].update(range=[pmin,pmaxd])
		fig["layout"][nyaxis].update(title="Efectividad (%)",range=[emin,emax])
	elif panelest[i]=="fluidez":
		fig.append_trace(mediafi,i+1,1)
		fig.append_trace(mediafd,i+1,1)
		for j in range(nz):
			fig.append_trace(lineavf[j],i+1,1)
		fig["layout"]["annotations"] += tuple(textozf)
		fig["layout"][nxaxis].update(range=[pmin,pmaxd])
		fig["layout"][nyaxis].update(title="Fluidez (%)",range=[fmin,fmax])
	if i==(npan-1):
		fig["layout"][nxaxis].update(title="Potencia neta (W)")
dibujo = plotly.offline.plot(fig,filename=nomfichhtml,auto_open=False)
#Muestro el dibujo
GC.webpage(Path(nomfichhtml).as_uri())

#Copyright (c) 2019-2022, José Luis García Pallero, jgpallero@gmail.com
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#   Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#   Neither the name of copyright holders nor the names of its contributors may
#   be used to endorse or promote products derived from this software without
#   specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
